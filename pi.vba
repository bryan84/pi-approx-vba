Sub generate()
    While Range("n2").Value < 3.1415 Or Range("n2").Value > 3.1416
        Call nextline
    Wend
    If Range("n2").Value > 3.1415 And Range("n2").Value < 3.1416 Then Range("n3").Value = "COMPLETE"
End Sub

Sub firstline()
' generates two random numbers in A2 and B2
    Range("A2").Select
    ActiveCell.FormulaR1C1 = "=RAND()"
    Range("B2").Select
    ActiveCell.FormulaR1C1 = "=RAND()"
' copy random numbers from A2 and B2 to permanent values in D2 and E2
    Range("A2").Select
    Selection.Copy
    Range("D2").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Range("B2").Select
    Application.CutCopyMode = False
    Selection.Copy
    Range("E2").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
' perform pythagorean calculation on the two values
    Range("G2").Select
    Application.CutCopyMode = False
    ActiveCell.FormulaR1C1 = "=SQRT(RC[-3]^2+RC[-2]^2)"
' determine if this value is inside or outside the unit circle
    Range("i2").Value = "=IF(RC[-2]<=1, TRUE, FALSE)"
' selects the required cell in order to call "generate" function, which requires a certain cell to be active
    Range("i2").Select
End Sub

Sub nextline()
    ActiveCell.Offset(0, -8).Range("A1").Select
    ActiveCell.FormulaR1C1 = "=RAND()"
    ActiveCell.Offset(0, 1).Range("A1").Select
    ActiveCell.FormulaR1C1 = "=RAND()"
    ActiveCell.Offset(0, -1).Range("A1:B1").Select
    Selection.Copy
    ActiveCell.Offset(0, 3).Range("A1").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    ActiveCell.Offset(0, 3).Range("A1").Select
    Application.CutCopyMode = False
    ActiveCell.FormulaR1C1 = "=SQRT(RC[-3]^2+RC[-2]^2)"
    ActiveCell.Offset(0, 2).Range("A1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-2]<=1, TRUE, FALSE)"
    ActiveCell.Offset(1, 0).Range("A1").Select
End Sub

Sub clear()
    Range("A:N").clear
End Sub

Sub headers()
' fills in all column titles
    Range("a1").Value = "x"
    Range("b1").Value = "y"
    Range("d1").Value = "x"
    Range("e1").Value = "y"
    Range("g1").Value = "pythag calc"
    Range("i1").Value = "inside?"
    Range("k1").Value = "# true"
    Range("l1").Value = "# false"
    Range("n1").Value = "approximation"
' adds formulas for counting trues and falses, then the formula for pi approximation
    Range("k2").Value = "=COUNTIF(I:I, TRUE)"
    Range("l2").Value = "=COUNTIF(I:I, FALSE)"
    Range("n2").Value = "=4*K2/(K2+L2)"
End Sub

Sub automate()
    While 1
        Call headers
        Call firstline
        Call generate
        Range("n3").Select
        Call clear
    Wend
End Sub
